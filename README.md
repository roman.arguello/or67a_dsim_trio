# Or67a evolution in D. melanogaster and the D. simulans clade

## This repository contains the scripts and workflow files associated with the paper "Odorant receptor copy number change, co-expression, and positive selection establish peripheral coding differences between fly species"

-----

## Directories:

- **poly_div_analyses**: This directory contains a jupyter notebook with the analyses polymorphism analsyes and MK test. It also contains all input/output files.


- **mol_evo**: 
    - **CODEML**: This direcotry contains the files and analyses underlying the protein tree inference and the dN/dS analyses
    - **syntenic_analysis**: aligment files and repeatmasker results used for the analyses of the regions containing Or67a genes
    

- **SSR**: This directory contains the R code and data files for the analysis and plotting of the electrophysiology data.


- **phylogenetics**: This directory contains the Mr.Bayes alignment and output.
------






