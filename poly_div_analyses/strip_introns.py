from Bio import SeqIO, AlignIO
import csv
import numpy as np
from Bio import SeqIO, AlignIO
##from Bio.codonalign import CodonSeq
import argparse
from collections import Counter
#import itertools
#from itertools import combinations  
###################  pares the command line
parser = argparse.ArgumentParser(
    description=
    'script that will calculate the number of synonmous and nonsynonymous positions in a refrence gene fasa, given a gff file'
)
parser.add_argument('-f',
                    '--fasta',
                    required=True,
                    type=str,
                    help="name of the reference gene region if fasta format")

parser.add_argument('-g',
                    '--gff',
                    required=True,
                    type=str,
                    help="gff file corresponding to the gene. needs to have exon column")


args = parser.parse_args()

###################
print("# commands: ", args)

# initialize the vector for the start/stop of exons
my_exons = [] 

# read in the gff file
with open(args.gff, newline = '') as gfeat:                                                                                          
    genfeats = csv.reader(gfeat, delimiter='\t')
    j=0;
    for feat in genfeats:
        if feat[2] == "exon":
            exStart = int(feat[3])-1
            exEnd = int(feat[4])-1
            my_exons.append((exStart,exEnd))
            j = j + 1



# now deal w/ the fasta sequence
fasta_sequences = list(SeqIO.parse(args.fasta, "fasta"))

for fasta in fasta_sequences:
    name, sequence = fasta.id, str(fasta.seq)
    print(">"+name)

    #prep for cDNA list
    my_cDNA = []

    for i in range(0, len(my_exons)):
        nextExon = str(sequence[int(my_exons[i][0]):int(my_exons[i][1])])
        my_cDNA.append(nextExon)
        #print(nextExon)
    # make the list a string
    cDNA = ''.join(my_cDNA)
    print(cDNA)