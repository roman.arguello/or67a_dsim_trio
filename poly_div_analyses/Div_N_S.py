from Bio import SeqIO, AlignIO
#from Bio import Seq
from Bio.Data import CodonTable
import csv
import numpy as np
from Bio import SeqIO, AlignIO
from Bio.codonalign import CodonSeq
import argparse
from collections import Counter
import collections
import statistics
from statistics import mean
#from Bio.Alphabet import generic_dna, generic_protein
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment
from Bio.codonalign import build
#from Bio.Alphabet import generic_dna
from Bio.Seq import Seq

###################  pares the command line ###################

parser = argparse.ArgumentParser(
    description=
    'script that will identify the positions that are fixed difference between an alignment of two species'
)
parser.add_argument('-fd',
                    '--fastaDNA',
                    required=True,
                    type=str,
                    help="fasta file of cDNA seqs to be aligned")

parser.add_argument('-l',
                    '--length',
                    required=True,
                    type=int,
                    help="length of the alingemnt; should be same for all seqs and will include gaps")

parser.add_argument('-s',
                    '--sample',
                    required=True,
                    type=int,
                    help="number of samples in first sample of species")

parser.add_argument('-t',
                    '--totalsample',
                    required=True,
                    type=int,
                    help="total number of samples")                    


args = parser.parse_args()

###################
print("# commands: ", args)

# set up a function that will count S (# potential syn. pos in a codon) and N (# of potential nonsyn. positions in a codon)
# ratios are S, N
gencode = {
    'ATA':[1/3,8/3], 'ATC':[1/3,8/3], 'ATT':[1/3,8/3], 'ATG':[0,3],
    'ACA':[1/3,8/3], 'ACC':[1/3,8/3], 'ACG':[1/3,8/3], 'ACT':[1/3,8/3],
    'AAC':[1/3,8/3], 'AAT':[1/3,8/3], 'AAA':[1/3,8/3], 'AAG':[1/3,8/3],
    'AGC':[1,2], 'AGT':[1,2], 'AGA':[2/3,7/3], 'AGG':[2/3,7/3],
    'CTA':[2/3,7/3], 'CTC':[2/3,7/3], 'CTG':[2/3,7/3], 'CTT':[2/3,7/3],
    'CCA':[1/3,8/3], 'CCC':[1/3,8/3], 'CCG':[1/3,8/3], 'CCT':[1/3,8/3],
    'CAC':[1/3,8/3], 'CAT':[1/3,8/3], 'CAA':[1/3,8/3], 'CAG':[1/3,8/3],
    'CGA':[2/3,7/3], 'CGC':[2/3,7/3], 'CGG':[2/3,7/3], 'CGT':[2/3,7/3],
    'GTA':[1/3,8/3], 'GTC':[1/3,8/3], 'GTG':[1/3,8/3], 'GTT':[1/3,8/3],
    'GCA':[1/3,8/3], 'GCC':[1/3,8/3], 'GCG':[1/3,8/3], 'GCT':[1/3,8/3],
    'GAC':[1/3,8/3], 'GAT':[1/3,8/3], 'GAA':[1/3,8/3], 'GAG':[1/3,8/3],
    'GGA':[1/3,8/3], 'GGC':[1/3,8/3], 'GGG':[1/3,8/3], 'GGT':[1/3,8/3],
    'TCA':[1,2], 'TCC':[1,2], 'TCG':[1,2], 'TCT':[1,2],
    'TTC':[1/3,8/3], 'TTT':[1/3,8/3], 'TTA':[2/3,7/3], 'TTG':[2/3,7/3],
    'TAC':[1/3,8/3], 'TAT':[1/3,8/3], 'TAA':[2/3,7/3], 'TAG':[2/3,7/3],
    'TGC':[1/3,8/3], 'TGT':[1/3,8/3], 'TGA':[2/3,7/3], 'TGG':[0,3]}

# 0 = nonsyn pos; 1 = syn pos
synpos = {
    'ATA':[0,0,1], 'ATC':[0,0,1], 'ATT':[0,0,1], 'ATG':[0,0,0],
    'ACA':[0,0,1], 'ACC':[0,0,1], 'ACG':[0,0,1], 'ACT':[0,0,1],
    'AAC':[0,0,1], 'AAT':[0,0,1], 'AAA':[0,0,1], 'AAG':[0,0,1],
    'AGC':[1,1,1], 'AGT':[1,1,1], 'AGA':[1,0,1], 'AGG':[1,0,1],
    'CTA':[1,0,1], 'CTC':[1,0,1], 'CTG':[1,0,1], 'CTT':[1,0,1],
    'CCA':[0,0,1], 'CCC':[0,0,1], 'CCG':[0,0,1], 'CCT':[0,0,1],
    'CAC':[0,0,1], 'CAT':[0,0,1], 'CAA':[0,0,1], 'CAG':[0,0,1],
    'CGA':[1,0,1], 'CGC':[1,0,1], 'CGG':[1,0,1], 'CGT':[1,0,1],
    'GTA':[0,0,1], 'GTC':[0,0,1], 'GTG':[0,0,1], 'GTT':[0,0,1],
    'GCA':[0,0,1], 'GCC':[0,0,1], 'GCG':[0,0,1], 'GCT':[0,0,1],
    'GAC':[0,0,1], 'GAT':[0,0,1], 'GAA':[0,0,1], 'GAG':[0,0,1],
    'GGA':[0,0,1], 'GGC':[0,0,1], 'GGG':[0,0,1], 'GGT':[0,0,1],
    'TCA':[1,1,1], 'TCC':[1,1,1], 'TCG':[1,1,1], 'TCT':[1,1,1],
    'TTC':[0,0,1], 'TTT':[0,0,1], 'TTA':[1,0,1], 'TTG':[1,0,1],
    'TAC':[0,0,1], 'TAT':[0,0,1], 'TAA':[0,1,1], 'TAG':[0,1,1],
    'TGC':[0,0,1], 'TGT':[0,0,1], 'TGA':[0,1,1], 'TGG':[0,0,0]}


# make functions that will use these dictionaries above
def get_NS_counts(thecodon):
    v = gencode[str(thecodon)]
    return v

def get_NS_pos(thecodon):
    p = synpos[str(thecodon)]
    return p


### function to count
def count(list1, l, r): 
      
    # x for x in list1 is same as traversal in the list 
    # the if condition checks for the number of numbers in the range  
    # l to r  
    # the return is stored in a list 
    # whose length is the answer 
    return len(list(x for x in list1 if l <= x <= r)) 


#############################################
# deal w/ fasta sequence
#############################################
fasta_sequences = list(SeqIO.parse(args.fastaDNA, "fasta"))
fl = 0
fixed_diff_pos = []
fixed_diff_vars = []

while fl < args.length:
    aln_nuc = []
    res = []
    for fasta in fasta_sequences:
        name, myDNA = fasta.id, str(fasta.seq)
        myDNA = Seq(myDNA)
        aln_nuc.append(myDNA[fl])

    totV = len(set(aln_nuc))
    
    # if there are more than one nucleotide at the position then continue
    if totV > 1:

        # loop over the alleles, order: dmel then dsim
        allele_count_sp1 = []
        allele_count_sp2 = []
        for i in range(totV):
            theVars = list(Counter(aln_nuc).keys())
            res = [index for index, value in enumerate(aln_nuc) if value == theVars[i]]
            sp1_specific = count(res, 0, (args.sample-1))
            sp2_specific = count(res, (args.sample), (args.totalsample-1))
            allele_count_sp1.append(sp1_specific)
            allele_count_sp2.append(sp2_specific)

        #print(fl, " <---- position")
        #print(theVars)
        #print("allele count sp1: ", allele_count_sp1)
        #print("allele count sp2: ", allele_count_sp2)
        
        # for biallelic pos
        if len(allele_count_sp1)==2:
            if (allele_count_sp1[0] + allele_count_sp2[0]==args.sample) & (allele_count_sp1[1] + allele_count_sp2[1]==(args.totalsample-args.sample)):
                #print("fixed diff")
                fixed_diff_pos.append(fl)
                fixed_diff_vars.append(theVars)

        # for triallelic pos
        if len(allele_count_sp1)==3:
            if (allele_count_sp1[0] + allele_count_sp2[0]==args.sample) or (allele_count_sp1[2] + allele_count_sp2[2]==(args.totalsample-args.sample)):
                #print("fixed diff")
                fixed_diff_pos.append(fl)
                fixed_diff_vars.append(theVars)
        

    fl = fl + 1


#print(fixed_diff_vars)

#############################################
# now get out a single fasta sequence and get positions of syn and nonsyn
#############################################

fasta_sequences2 = list(SeqIO.parse(args.fastaDNA, "fasta"))
reqseq = 0
codonstart = 0
myS = 0
myN = 0
SynPos = []

while reqseq == 0:
    
    for fasta in fasta_sequences:
        defseq = str(fasta.seq)
        NonSynPos = list(range(0, len(defseq)))
        
        while (codonstart <= len(defseq)-1): 
            singlecodon = CodonSeq(str(defseq[codonstart:codonstart+3]))
            tPos = get_NS_pos(singlecodon)
            res = [key for key, val in enumerate(tPos) if val == 1] 
            
            if (len(res) > 0):
                new_res = [x+codonstart for x in res]
                SynPos = SynPos + new_res

            codonstart = codonstart+3

    reqseq = reqseq + 1

# remove the syn from the full set of positions to get the nonsyn.
NonSynPos = [x for x in NonSynPos if x not in SynPos]

# the positons that are syn and nosyn
print("total div: ", len(fixed_diff_pos))
synDiv = {element for element in SynPos if element in fixed_diff_pos}
print("N.syn: ", len(synDiv))
print(sorted(synDiv))
nonsynDiv = {element for element in NonSynPos if element in fixed_diff_pos}
print("N.nonsyn: ", len(nonsynDiv))
print(sorted(nonsynDiv))
