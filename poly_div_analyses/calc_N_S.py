from Bio import SeqIO, AlignIO
import csv
import numpy as np
import sys
from Bio import SeqIO, AlignIO
from Bio.codonalign import CodonSeq
import argparse
from collections import Counter
import itertools
from itertools import combinations  
import collections
import statistics
from statistics import mean

###################  pares the command line ###################
parser = argparse.ArgumentParser(
    description=
    'script that will calculate the number of synonmous and nonsynonymous positions in a refrence gene fasta, given a gff file'
)
parser.add_argument('-f',
                    '--fasta',
                    required=True,
                    type=str,
                    help="name of the reference gene region if fasta format")

parser.add_argument('-a',
                    '--alnfa',
                    required=True,
                    type=str,
                    help="name of the aligned fasta file")                    

parser.add_argument('-g',
                    '--gff',
                    required=True,
                    type=str,
                    help="gff file corresponding to the gene. needs to have exon column")

parser.add_argument('-nf',
                    '--newfasta',
                    required=True,
                    type=str,
                    help="TRUE or FALSE to indicate if the syn/nonsyn fasta files should be outputted")                    


args = parser.parse_args()

###################
print("# commands: ", args)
# set up a function that will count S (# potential syn. pos in a codon) and N (# of potential nonsyn. positions in a codon)
# ratios are S, N
gencode = {
    'ATA':[1/3,8/3], 'ATC':[1/3,8/3], 'ATT':[1/3,8/3], 'ATG':[0,3],
    'ACA':[1/3,8/3], 'ACC':[1/3,8/3], 'ACG':[1/3,8/3], 'ACT':[1/3,8/3],
    'AAC':[1/3,8/3], 'AAT':[1/3,8/3], 'AAA':[1/3,8/3], 'AAG':[1/3,8/3],
    'AGC':[1,2], 'AGT':[1,2], 'AGA':[2/3,7/3], 'AGG':[2/3,7/3],
    'CTA':[2/3,7/3], 'CTC':[2/3,7/3], 'CTG':[2/3,7/3], 'CTT':[2/3,7/3],
    'CCA':[1/3,8/3], 'CCC':[1/3,8/3], 'CCG':[1/3,8/3], 'CCT':[1/3,8/3],
    'CAC':[1/3,8/3], 'CAT':[1/3,8/3], 'CAA':[1/3,8/3], 'CAG':[1/3,8/3],
    'CGA':[2/3,7/3], 'CGC':[2/3,7/3], 'CGG':[2/3,7/3], 'CGT':[2/3,7/3],
    'GTA':[1/3,8/3], 'GTC':[1/3,8/3], 'GTG':[1/3,8/3], 'GTT':[1/3,8/3],
    'GCA':[1/3,8/3], 'GCC':[1/3,8/3], 'GCG':[1/3,8/3], 'GCT':[1/3,8/3],
    'GAC':[1/3,8/3], 'GAT':[1/3,8/3], 'GAA':[1/3,8/3], 'GAG':[1/3,8/3],
    'GGA':[1/3,8/3], 'GGC':[1/3,8/3], 'GGG':[1/3,8/3], 'GGT':[1/3,8/3],
    'TCA':[1,2], 'TCC':[1,2], 'TCG':[1,2], 'TCT':[1,2],
    'TTC':[1/3,8/3], 'TTT':[1/3,8/3], 'TTA':[2/3,7/3], 'TTG':[2/3,7/3],
    'TAC':[1/3,8/3], 'TAT':[1/3,8/3], 'TAA':[2/3,7/3], 'TAG':[2/3,7/3],
    'TGC':[1/3,8/3], 'TGT':[1/3,8/3], 'TGA':[2/3,7/3], 'TGG':[0,3]}

# 0 = nonsyn pos; 1 = syn pos
synpos = {
    'ATA':[0,0,1], 'ATC':[0,0,1], 'ATT':[0,0,1], 'ATG':[0,0,0],
    'ACA':[0,0,1], 'ACC':[0,0,1], 'ACG':[0,0,1], 'ACT':[0,0,1],
    'AAC':[0,0,1], 'AAT':[0,0,1], 'AAA':[0,0,1], 'AAG':[0,0,1],
    'AGC':[1,1,1], 'AGT':[1,1,1], 'AGA':[1,0,1], 'AGG':[1,0,1],
    'CTA':[1,0,1], 'CTC':[1,0,1], 'CTG':[1,0,1], 'CTT':[1,0,1],
    'CCA':[0,0,1], 'CCC':[0,0,1], 'CCG':[0,0,1], 'CCT':[0,0,1],
    'CAC':[0,0,1], 'CAT':[0,0,1], 'CAA':[0,0,1], 'CAG':[0,0,1],
    'CGA':[1,0,1], 'CGC':[1,0,1], 'CGG':[1,0,1], 'CGT':[1,0,1],
    'GTA':[0,0,1], 'GTC':[0,0,1], 'GTG':[0,0,1], 'GTT':[0,0,1],
    'GCA':[0,0,1], 'GCC':[0,0,1], 'GCG':[0,0,1], 'GCT':[0,0,1],
    'GAC':[0,0,1], 'GAT':[0,0,1], 'GAA':[0,0,1], 'GAG':[0,0,1],
    'GGA':[0,0,1], 'GGC':[0,0,1], 'GGG':[0,0,1], 'GGT':[0,0,1],
    'TCA':[1,1,1], 'TCC':[1,1,1], 'TCG':[1,1,1], 'TCT':[1,1,1],
    'TTC':[0,0,1], 'TTT':[0,0,1], 'TTA':[1,0,1], 'TTG':[1,0,1],
    'TAC':[0,0,1], 'TAT':[0,0,1], 'TAA':[0,1,1], 'TAG':[0,1,1],
    'TGC':[0,0,1], 'TGT':[0,0,1], 'TGA':[0,1,1], 'TGG':[0,0,0]}


# make functions that will use these dictionaries
def get_NS_counts(thecodon):
    v = gencode[str(thecodon)]
    return v

def get_NS_pos(thecodon):
    p = synpos[str(thecodon)]
    return p




# initialize the vector for the start/stop of exons
my_exons = [] 

# read in the gff file
with open(args.gff, newline = '') as gfeat:                                                                                          
    genfeats = csv.reader(gfeat, delimiter='\t')
    j=0;
    for feat in genfeats:
        if feat[2] == "exon":
            exStart = int(feat[3])-1
            exEnd = int(feat[4])-1
            my_exons.append((exStart,exEnd))
            j = j + 1

#print(my_exons)

#############################################
# now deal w/ the ref fasta sequence
#############################################
fasta_sequences = list(SeqIO.parse(args.fasta, "fasta"))

#prep for cDNA list
my_cDNA = []

for fasta in fasta_sequences:
    name, sequence = fasta.id, str(fasta.seq)
    #print(name)
    for i in range(0, len(my_exons)):
        nextExon = str(sequence[int(my_exons[i][0]):int(my_exons[i][1])])
        my_cDNA.append(nextExon)
        #print(nextExon)
# make the list a string
cDNA = ''.join(my_cDNA)
#print(cDNA)
#print(cDNA.translate)


# make it a codon object
Mycodon = CodonSeq(cDNA)

# loop over triplets and count the S and N
# and also collect the positions that are S or N
codonstart = 0
myS = 0
myN = 0
SynPos = []
NonSynPos = list(range(0, len(cDNA)))

while (codonstart <= len(cDNA)-1): 
    singlecodon = CodonSeq(str(Mycodon[codonstart:codonstart+3]))
    s,n = get_NS_counts(singlecodon)
    tPos = get_NS_pos(singlecodon)
    res = [key for key, val in enumerate(tPos) if val == 1] 
    if (len(res) > 0):
        new_res = [x+codonstart for x in res]
        SynPos = SynPos + new_res
    myS = myS + s
    myN = myN + n
    codonstart = codonstart+3



#print(cDNA)
#print(len(SynPos))
# remove SynPos from the total number of positions of cDNA
NonSynPos = [x for x in NonSynPos if x not in SynPos]
#print(NonSynPos)
#print(SynPos)

#############################################
# now deal with the alignment and see what varies
# now deal w/ the ref fasta sequence
#############################################
fasta_alns = list(SeqIO.parse(args.alnfa, "fasta"))

# loop over positions across all samples
nucpos = 0
poly_pos = []
pi_pos = []
Nsamp = 0
# loop over the full cDNA sequence, sliding along base by base

# for each base
while nucpos <= len(cDNA)-1:
    nucState = []

    # loop over the individual samples
    for alnfa in fasta_alns:
        nucState.append(alnfa.seq[nucpos])
        
        if (nucpos == 0):
            Nsamp = Nsamp+1

    # analyze positions that have > 1 variant (ie is polymorphic)
    totalDiffs = 0 # counting pairwise diffs
    if len(set(nucState)) > 1:
        poly_pos.append(nucpos)
        
        #print(nucpos)
        #print(nucState)
        #print(Counter(nucState).keys())
        AFreq=collections.Counter(nucState)
        #print(AFreq)
        AFreqV = list(AFreq.values())
        AF1 = AFreqV[0]/Nsamp
        AF2 = AFreqV[1]/Nsamp
        #AF1 = AFreqV[0]
        #AF2 = AFreqV[1]
        #print(AF1,AF2)
        minAF = min(AF1,AF2)
        #print(minAF, "   <---")
        # get all pair-wise comparisons
        pairWise = list(itertools.combinations(nucState, 2))
        #print("total PW: ", len(pairWise))
        # count how many pair-wise comp. is different
        for pw in pairWise:
            if (pw[0]!=pw[1]):
                totalDiffs = totalDiffs + 1
        # get pi, the average pair-wide diff. over all comparisons
        pi = (totalDiffs/len(pairWise))
        #pi = totalDiffs * AF1 * AF2
        #pi = (2*minAF*(Nsamp-minAF))/(Nsamp*(Nsamp-1))
        #print("total dif: ", totalDiffs)
        #print("-->pi: ", pi)
        pi_pos.append(pi)
    
    nucpos = nucpos + 1

#print(poly_pos)
#print(type(SynPos))
#print(pi_pos)
#print("# of poly positions: ", len(poly_pos))

# divide the polymorphic sites into syn. and nonsyn. 
geneSynPos = list(set(poly_pos) & set(SynPos))
geneNonSynPos = list(set(poly_pos) & set(NonSynPos))



#print(int(geneSynPos[0]))
#print(pi_pos[int(geneSynPos[0])])

S_index = []
N_index = []


# loop over the total set of polymorphic polistions at get out indices of the synon. set
fp = 0
for PP in poly_pos:
    for S in SynPos:
        if (PP == S):
            S_index.append(fp)
    fp = fp + 1

# loop over the total set of polymorphic polistions at get out indices of the nonsynon. set
fp = 0
for PP in poly_pos:
    for N in NonSynPos:
        if (PP == N):
            N_index.append(fp)
    fp = fp + 1


#print("\n--------")
print("# summary of: " + args.alnfa)
print("len.cDNA:\t",len(cDNA))
print("N.syn.pos:\t",myS)
print("N.nonsyn.pos:\t",myN)
print("tot.samps:\t",Nsamp)
print("pi.cDNA:\t",sum(pi_pos)/len(cDNA))

print("N.syn.poly.pos:\t",len(geneSynPos))
print("N.nonsyn.poly.pos:\t",len(geneNonSynPos))


Pi_S_list = [pi_pos[i] for i in S_index] 
Pi_syn = mean(Pi_S_list)/myS
#print("meanS: ", mean(Pi_S_list))
print("pi.s:\t",Pi_syn)

Pi_N_list = [pi_pos[i] for i in N_index] 
Pi_nonsyn = mean(Pi_N_list)/myN
print("pi.ns:\t",Pi_nonsyn)

print("pi.ns/pi.s:\t",Pi_nonsyn/Pi_syn)
#print("--------\n\n")



# test to see if output fastas should be made
if args.newfasta == "TRUE": 

    #############################################
    # use the above N and S indicies to get out the 
    # nucleotide sequences from the fasta files and 
    # write new N and S fasta files
    #############################################

    # get the fasta postions out
    S_pos_fasta = [poly_pos[i] for i in S_index]
    N_pos_fasta = [poly_pos[i] for i in N_index]
    #print("syn:")
    #print(S_pos_fasta)
    #print("nonsyn:")
    #print(N_pos_fasta)


    # prep the new output files
    SoutFile = (args.alnfa)
    SoutFile = SoutFile.replace(".fasta", "_Spos.fasta")

    # prep the new output files
    NoutFile = (args.alnfa)
    NoutFile = NoutFile.replace(".fasta", "_Npos.fasta")

    ######
    # make Syn Fasta file
    ######
    fasta_to_edit = list(SeqIO.parse(args.alnfa, "fasta"))
    # loop over positions across all samples
    nuc = 0
    Nsamp = 0
    # open/create new fasta file
    f = open(SoutFile, "w")

    # loop over each fasta entry to get syn. bases
    # write to a new fasta file
    for fastaE in fasta_to_edit:
        name, sequence = str(fastaE.id), str(fastaE.seq)
        nuc = 0
        S_base = []
        # scan over the seq
        for bp in sequence:
            # if the position matches a syn. pos then save it 
            if nuc in S_pos_fasta:
                S_base.append(bp)
            nuc = nuc + 1

        Sbases = ''.join(S_base)
        #print(">" + name)
        #print(Sbases)
        f.write(">" + name)
        f.write("\n")
        f.write(Sbases)
        f.write("\n")

    ######
    # make NonSyn Fasta file
    ######
    fasta_to_edit2 = list(SeqIO.parse(args.alnfa, "fasta"))
    # loop over positions across all samples
    nuc = 0
    Nsamp = 0
    # open/create new fasta file
    f = open(NoutFile, "w")

    # loop over each fasta entry to get syn. bases
    # write to a new fasta file
    for fastaE in fasta_to_edit2:
        name, sequence = str(fastaE.id), str(fastaE.seq)
        nuc = 0
        N_base = []
        # scan over the seq
        for bp in sequence:
            # if the position matches a syn. pos then save it 
            if nuc in N_pos_fasta:
                N_base.append(bp)
            nuc = nuc + 1

        Nbases = ''.join(N_base)
        #print(">" + name)
        #print(Sbases)
        f.write(">" + name)
        f.write("\n")
        f.write(Nbases)
        f.write("\n")
