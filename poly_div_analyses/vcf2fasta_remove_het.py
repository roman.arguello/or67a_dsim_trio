import vcf
from Bio import SeqIO
import numpy as np
import argparse


###################  pares the command line
parser = argparse.ArgumentParser(
    description=
    'utility that writes VCF data to FASTA format using a reference genome'
)
parser.add_argument('-v',
                    '--vcf',
                    required=True,
                    type=str,
                    help="name of the vcf file to be read in")

parser.add_argument('-f',
                    '--fasta',
                    required=True,
                    type=str,
                    help="name of the reference genome if fasta format")

parser.add_argument('-c',
                    '--chr',
                    required=True,
                    type=str,
                    help="chromosome symbol (ie chr3R or Scf_3R)")

parser.add_argument('-s',
                    '--start',
                    required=True,
                    type=int,
                    help="coordinate to begin extraction from ref. genome")

parser.add_argument('-e',
                    '--end',
                    required=True,
                    type=int,
                    help="coordinate to end extraction from ref. genome")                    

parser.add_argument('-o',
                    '--out',
                    required=True,
                    type=str,
                    help="name of outputted FASTA file")


args = parser.parse_args()

###################







###########  begin with the VCF file ################
#####################################################
# header to add to the outputted fasta files (along w/ sample ID)
myHeader = str(args.chr) + "_" + str(args.start) + "_" + str(args.end)

# initialize array to be filled by variable positions
VarPos = []

# initialize array for sample IDs
MySamp = []

# index the number of positions
p = 0

# for sampling het sites
nSam=1
myprob=0.5

##### get the summary of the VCF file
vcf_reader1 = vcf.Reader(open(args.vcf, 'r'))

print("vcf file: ", args.vcf)
totSamps = len(vcf_reader1.samples)
print("total samples: ", totSamps)

totV = 0
for record in vcf_reader1:
    totV = totV + 1

print("total variants:", totV, "\n\n")
#####

# initialize the matrix to be filed by specific bases
GTs = np.empty((totSamps, totV), dtype='str')

##### start working w/ the VCF file now
vcf_reader = vcf.Reader(open(args.vcf, 'r'))

for record in vcf_reader:
    
    myChrom = record.CHROM
    #print(record.CHROM)
    # for het calls use IUPAC notation
    global pHET
    if (record.REF == "A" and record.ALT[0] == "G") or (record.REF == "G" and record.ALT[0] == "A"):
        myNucs = ["A","G"]
        s = np.random.binomial(nSam, myprob, 1)
        pHET = myNucs[int(s)]
    if (record.REF == "C" and record.ALT[0] == "T") or (record.REF == "T" and record.ALT[0] == "C"):   
        myNucs = ["C","T"]
        s = np.random.binomial(nSam, myprob, 1)
        pHET = myNucs[int(s)]
    if (record.REF == "G" and record.ALT[0] == "C") or (record.REF == "C" and record.ALT[0] == "G"):
        myNucs = ["G","C"]
        s = np.random.binomial(nSam, myprob, 1)
        pHET = myNucs[int(s)]
    if (record.REF == "A" and record.ALT[0] == "T") or (record.REF == "T" and record.ALT[0] == "A"):
        myNucs = ["A","T"]
        s = np.random.binomial(nSam, myprob, 1)
        pHET = myNucs[int(s)]
    if (record.REF == "G" and record.ALT[0] == "T") or (record.REF == "T" and record.ALT[0] == "G"):
        myNucs = ["G","T"]
        s = np.random.binomial(nSam, myprob, 1)
        pHET = myNucs[int(s)]
    if (record.REF == "A" and record.ALT[0] == "C") or (record.REF == "C" and record.ALT[0] == "A"):
        myNucs = ["A","C"]
        s = np.random.binomial(nSam, myprob, 1)
        pHET = myNucs[int(s)]
    
    # fill the position array
    VarPos.append(record.POS)
    
    # index the individuals per position
    i=0

    for sample in record.samples:
        
        #fill the smaple array
        if p==0:
            MySamp.append(sample.sample)

        # assign the nuclotide based on the VCF format
        if sample['GT'] == "0/0":
            pV = record.REF[0]
        if sample['GT'] == "1/1":
            pV = record.ALT[0]
        if sample['GT'] == "./.":
            pV = record.REF[0]
        if sample['GT'] == "0/1":
            pV = pHET

        GTs[i,p] = pV
        #print(GTs)
        i=i+1
    p = p + 1
    
#print(VarPos)
#print(MySamp)
#print(GTs[1,])
#print(GTs.shape)
#np.savetxt('GTstest.txt', GTs)
#print(myChrom)
#np.savetxt('testGTs.txt', GTs, delimiter=" ", fmt="%s") 


###########  now process the FASTA file #############
#####################################################

for record in SeqIO.parse(args.fasta, "fasta"):
    #print(record.id)
    if record.id==args.chr:
        targetSEQ = record.seq

# prep output file
file = open(args.out, "w")

# loop over every sample and every position
for MySamps in range(0,GTs.shape[0]):
    UpSeq = targetSEQ # for each sample copy the ref, which will be modifed uniquely for each sample
    mutable_UpSeq = UpSeq.tomutable() # make the fasta sequence 'mutable' so changes can be made

    
    for vp in range(0,len(VarPos)):
        #print(vp)
        extSeq = VarPos[vp] - 1
        mutable_UpSeq[extSeq] = GTs[MySamps,vp]
    
    # switch the sequences and print
    GeneRegion = mutable_UpSeq[args.start:args.end]
    myheader = ">"+MySamp[MySamps]+ "_" + myHeader + "\n"
    file.write(myheader)
    file.write(str(GeneRegion))
    file.write("\n")
    
file.close()